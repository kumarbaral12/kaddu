package com.kumar;

import java.sql.*;
import java.util.List;

public interface StudentInterface{
	
	List<Student> selectData();
	Student selectDataById(int id) throws Exception;
	void insertData(Student student) throws Exception;
	void updateData(int id, Student std) throws Exception;
	void deleteDataById(int id ) throws Exception;
	void close(Connection con, Statement st, ResultSet rs);

}
