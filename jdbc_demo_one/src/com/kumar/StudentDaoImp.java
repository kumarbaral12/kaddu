package com.kumar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StudentDaoImp implements StudentInterface {

	private String url = "jdbc:mysql://localhost:3306/web_student_tracker?serverTimezone=UTC";
	private String user = "webstudent";
	private String pass = "webstudent";
	private Connection con = null;
	private Statement st = null;
	private ResultSet rs = null;
	private PreparedStatement pst = null;

	@Override
	public List<Student> selectData() {
		List<Student> list = new ArrayList<>();

		try {
			con = DriverManager.getConnection(url, user, pass);
			String query = "select * from student";

			st = con.createStatement();
			rs = st.executeQuery(query);

			while (rs.next()) {

				int id = rs.getInt("id");
				String fName = rs.getString("first_name");
				String lName = rs.getString("last_name");
				int ssn = rs.getInt("ssn");
				String email = rs.getString("email");

				Student std = new Student(id, fName, lName, ssn, email);
				list.add(std);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(con, st, rs);
		}

		return list;
	}

	@Override
	public Student selectDataById(int id) throws Exception {
		con = DriverManager.getConnection(url, user, pass);
		String query = "select * from student where id=" + id;
		st = con.createStatement();
		rs = st.executeQuery(query);
		rs.next();
		Student std = new Student(id, rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));

		close(con, st, rs);

		return std;
	}
	public int randNum() {
		StringBuilder sb = new StringBuilder(); 
		
		Random rd  = new Random(); 
		for(int i=0; i< 9; i++) {
			
			int rand = rd.nextInt(9); 
			
			sb.append(rand);
			
		}
		int ssn = Integer.valueOf(sb.toString());
				
		return ssn;
		
	}
		

	@Override
	public void insertData(Student student) throws Exception {

		con = DriverManager.getConnection(url, user, pass);
		String query = "insert into student values (?,?,?,?,?)";

		pst = con.prepareStatement(query);
		pst.setInt(1, student.getId());
		pst.setString(2, student.getFirstName());
		pst.setString(3, student.getLastName());
		pst.setInt(4, student.getSsn());
		pst.setString(5, student.getEmail());

		pst.executeUpdate();

		close(con, pst, null);

	}
	public void newTableInsert( Student student) throws Exception{
	con = DriverManager.getConnection(url, user, pass);
	String query = "insert into studentBishnu values (?,?,?)";

	pst = con.prepareStatement(query);
	
	pst.setString(1, student.getFirstName());
	pst.setString(2, student.getLastName());
	pst.setInt(3, student.getSsn());

	pst.executeUpdate();
	close(con, pst, null);

}

	@Override
	public void updateData(int id, Student student) throws Exception {
		con = DriverManager.getConnection(url, user, pass);
		String query = "update student set first_name = ?, last_name = ?, email = ? where id = " + id;
		pst = con.prepareStatement(query);
		
		pst.setString(1, student.getFirstName());
		pst.setString(2, student.getLastName());
		pst.setString(3, student.getEmail());
		
		pst.executeUpdate();

		close(con, pst, null);
		
	}

	@Override
	public void deleteDataById(int id) throws Exception {
		con = DriverManager.getConnection(url, user, pass);
		String query = "delete from student where id=" + id;
		pst = con.prepareStatement(query);

		int row = pst.executeUpdate();
		
		System.out.println(row + " row/s affected.");

		close(con, pst, null);
	}

	@Override
	public void close(Connection con, Statement st, ResultSet rs) {
		try {
			if (con != null) {
				con.close();
			}
			if (st != null) {
				st.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
